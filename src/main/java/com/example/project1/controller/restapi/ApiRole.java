package com.example.project1.controller.restapi;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.project1.model.entity.RoleEntity;
import com.example.project1.repository.RoleRepository;

@RestController
@RequestMapping("/api/role")
public class ApiRole {

	@Autowired
	private RoleRepository roleRepository;

	@GetMapping("/getAll")
	public List<RoleEntity> getAll() {
		return roleRepository.findAll();
	}

	@PostMapping
	public RoleEntity save(@RequestBody RoleEntity role) {
//		User userLogin = UserAuth.getUser();
		Date currentDate = new Date();

		role.setCreatedBy("admin");
		role.setCreatedDate(currentDate);

		return roleRepository.save(role);
	}

	@GetMapping("/getRoleById/{idRole}")
	public RoleEntity getByIdRole(@PathVariable Integer idRole) {
		return roleRepository.findAllByIdRole(idRole);
	}

	@PutMapping("/edit/{id}") // untuk edit role
	public ResponseEntity<RoleEntity> updateRoleUser(@PathVariable Integer id, @Validated @RequestBody RoleEntity newestRole) {
//		User userLogin = UserAuth.getUser();
		Date currentDate = new Date();

		RoleEntity newRole = roleRepository.findById(id).get();

		newRole.setLastModifiedBy("admin");
		newRole.setLastModifiedDate(currentDate);
		newRole.setRole(newestRole.getRole());

		final RoleEntity updateRoleUser = roleRepository.save(newRole);
		return ResponseEntity.ok(updateRoleUser);
	}

	@DeleteMapping("/{idRole}")
	public void delete(@PathVariable Integer idRole) {
		roleRepository.deleteById(idRole);
	}

}
