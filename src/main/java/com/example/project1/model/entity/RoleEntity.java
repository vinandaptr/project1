package com.example.project1.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = RoleEntity.TABLE_NAME)
@Data
public class RoleEntity extends CommonEntity {
	public static final String TABLE_NAME = "t_role";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_role", length = 10)
	private Integer idRole;

	@Column(name = "role", length = 128)
	private String role;

}
