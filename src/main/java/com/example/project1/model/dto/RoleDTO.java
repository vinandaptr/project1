package com.example.project1.model.dto;

import lombok.Data;

@Data
public class RoleDTO {

	private String idRole;
	private String roleUser;

}
